import requests


def getMemberData(username, password):
    session_requests = requests.session() # create a persistent session to handle requests and cookies

    gpw_login_url = "https://gopaywall.com/login.php"
    gpw_csv_url = f"https://{username}.gopaywall.com/exportusers.php"
    gpw_payload = {
        "username": username,
        "password": password,
        "doLogin": 1
}

    # login to GoPayWall
    print("[scrape_gopaywall] Fetching GoPayWall user data...")
    data = session_requests.post(
        gpw_login_url, 
        data = gpw_payload, 
        headers = dict(referer = gpw_login_url),
        verify = False
    )

    # retrieve CSV file from GoPayWall
    data = session_requests.get(
        gpw_csv_url, 
        headers = dict(referer = gpw_csv_url),
        verify = False
    )

    print("[scrape_gopaywall] Cleaning GoPayWall csv data...")
    # split the downloaded file at each new line and add it to an array
    data = data.text.split('\n')
    # remove the keys of the csv file. We don't need them.
    data.pop(0)
    # remove the last row in the file, it's empty ['']
    data.pop()
    # this will hold the formatted data for use in automation
    gpw_member_data = [] 

    # loop through all records and separate each by commas and add to an array
    for row in data:
        row = row.split(',')
        # create record to store. strip literal quotes from each value, and replace nulls with "empty"
        empty = "empty"
        record = {
            "id" : row[0].strip('\"') or empty,
            "username" : row[1].strip('\"') or empty,
            "email": row[2].strip('\"') or empty,
            "fname" : row[3].strip('\"') or empty,
            "lname" : row[4].strip('\"') or empty,
            "membership_id" : row[5].strip('\"') or empty,
            "membership_expires" : row[6].strip('\"')
        }
        gpw_member_data.append(record)
    return gpw_member_data