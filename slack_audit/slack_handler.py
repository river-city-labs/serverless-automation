import slack
import requests
from datetime import date
from datetime import timedelta
from time import mktime

from shared_scripts.login_handler import LoginHandler


logins = LoginHandler.instance()
doorAccessChannel = 'GFHDX2TH7'

slackClient = slack.WebClient(token=logins.getSlackToken())


def sendSlackInvite(name, email):
    print(f"[slack_handler] sending slack invite to: {name} at {email}")
    expiration_date = date.today() + timedelta(days=29)
    slackUrl = 'https://slack.com/api/users.admin.invite'
    headers = {'Content-type': 'x-www-form-urlencoded'}
    data = dict(
        token = logins.getSlackLegacyToken(),
        email = email,
        channels = "C046ZF2N9,CBUV5G1TR,C046HEHJV,C10SQV6E5,C04TGQ183,C047M26JP,C04FMLWUQ,C0P78DTU4,C2PF6N5P0,C0KV08GTC,C3NEPSKS6,C045UA8NQ,C3XNQ1R4K,C04R49PC6,C7BSDAJUD,C046HEHJZ,CB7A74FQF,C33PTNYUQ,C5QQP8X7T",
        real_name = name,
        expiration_ts = mktime(expiration_date.timetuple()) # unix timestamp of today + 30 days
    )
    r = requests.post(slackUrl, data, headers)


def getSlackUsers(existing_members_array):
    print("[slack_handler] attempting to link slack users to gopaywall users...")
    response = slackClient.users_list(limit=999)
    response = response["members"]
    allSlackUsers = []
    names = []
    for user in response:
        if "email" in user["profile"].keys(): # filters out bots
            allSlackUsers.append({
                "slackEmail" : user["profile"]["email"] or "",
                "id" : user["id"]
            })
            names.append(user["profile"]["real_name"])

    for member in existing_members_array:
        try:
            response = slackClient.users_lookupByEmail(email=member["email"])
            member["slackID"] = response["user"]["id"]
            member["slackEmail"] = member["email"]
        except:
            # user is not in slack yet
            memberName = f"{member['fname']} {member['lname']}"
            try:
                index = names.index(memberName)
                member["slackID"] = allSlackUsers[index]["id"]
                member["slackEmail"] = allSlackUsers[index]["slackEmail"]
            except ValueError:
                pass

    return existing_members_array
         

def getAllDoorAccess(existing_members_array):
    # given an array of member objects, compile a list of all users in the door access channel, and determine if each member in the array is included or not
    print("[slack_handler] Fetching current members of the rcl-access channel...")
    current_channel_members = slackClient.conversations_members(channel = doorAccessChannel, limit="999")["members"] #rcl-access channel

    for member in existing_members_array:
        if member["slackID"] is not "": # if the member has valid slack id
            if member["slackID"] in current_channel_members:
                member["hasDoorAccess"] = "True"
    return existing_members_array


def addDoorAccess (member):
    print(f"[slack_handler] adding {member['fname']} {member['lname']} to the door code channel...")
    response = slackClient.conversations_invite(channel = doorAccessChannel, users = member["slackID"])