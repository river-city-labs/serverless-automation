import csv
import json
import datetime
import sys
import os

import scrape_gopaywall
import slack_handler
from shared_scripts.login_handler import LoginHandler
from shared_scripts.storage_handler import StorageHandler
from shared_scripts.mail_handler import EmailHandler
from shared_scripts.log_handler import LogHandler

def getCurrentUserData (logins):
    gpw_member_data = scrape_gopaywall.getMemberData("rivercitylabs", logins.getGoPayWallPass())
    gpw_member_data = formatData(gpw_member_data)
    gpw_member_data = slack_handler.getSlackUsers(gpw_member_data)
    gpw_member_data = slack_handler.getAllDoorAccess(gpw_member_data)
    return gpw_member_data

def formatData (members_array):
    print("Formatting member data for function use...")
    existing_members = []
    for user in members_array:
        existing_members.append({
            "id": user["id"],
            "fname": user["fname"],
            "lname": user["lname"],
            "email": user["email"].lower(),
            "membership_id": user["membership_id"],  
            "slackID": "",
            "slackEmail": "",
            "hasDoorAccess": "False",
            "addToDoorAccess": "",
            "inviteSent": "False"
            })
    return existing_members

def stripExtraData (members_array):
    for member in members_array:
        del member["fname"]
        del member["lname"]
        del member["email"]
        del member["membership_id"]
        del member["slackID"]
        del member["slackEmail"]
    return members_array


def handler(event, contex):
    try:
        if "LOCAL-DEVELOPMENT" not in os.environ: #toggle local development options
            sys.stdout = LogHandler.instance()

        logins = LoginHandler.instance()
        mailer = EmailHandler()

        jsonFile = 'slack_audit.json'
        current_data = None

        print( f'Checking if {jsonFile} already exists...')
        s3 = StorageHandler()
        if s3.JsonExists(jsonFile) is False:
            print("It doesnt, creating it...")

            initialData = {
                "lastUpdated": str(datetime.datetime.now())[:16],
                "members" : []
            }

            initialData["members"] = stripExtraData(getCurrentUserData(logins))
            s3.writeJson(initialData, jsonFile)
            current_data = getCurrentUserData(logins)
            print("Finished initial file creation. Continuing script as normal...")
        else:
            print("It does.")
            current_data = getCurrentUserData(logins)

        last_run_data = s3.readJson(jsonFile)

        # lists used for daily report
        invitesSent = []
        doorAccessGranted = []
        membersToRemove = []

        #add all membership checks here to make the most of function data
        print("Checking each user for actionable items...")
        for member in current_data:

            memberName = f"{member['fname']} {member['lname']}"

            # update member record with cached data from last run
            for existingUser in last_run_data["members"]:
                if existingUser['id'] == member['id']:
                    member['addToDoorAccess'] = existingUser['addToDoorAccess']
                    member['inviteSent'] = existingUser['inviteSent']

            # find members that need to be invited to slack
            if member["membership_id"] is not "0" and member["slackID"] is "" and member["inviteSent"] is "False":
                slack_handler.sendSlackInvite(member["fname"], member["email"])
                member["inviteSent"] = "True"
                invitesSent.append([memberName, member["email"]])

                # calculate future date to add user to door-access channel and save it
                member["addToDoorAccess"] = str(datetime.date.today() + datetime.timedelta(days=31))

            # find users that need to be added to the door access channel
            if member["hasDoorAccess"] is "False" and member["membership_id"] is not "0" and member["slackID"] is not "":
                if member["addToDoorAccess"] == str(datetime.date.today()):
                    slack_handler.addDoorAccess(member)
                    member["hasDoorAccess"] = "True"
                    doorAccessGranted.append([memberName, member["email"]])

            # find members that need to be removed from slack and door access
            # has SlackID but membershipID is 0 (add check for slack deactivated status, currently an endpoint doesn't exist)
            if member["membership_id"] is "0" and member["slackID"] is not "":
                print(f'{memberName} is in slack and currently has no subscription. Consider removing them.')
                membersToRemove.append([memberName, member["email"]])


        # write current member data back to S3
        last_run_data = {
                "lastUpdated": str(datetime.datetime.now())[:16],
                "members" : stripExtraData(current_data)
            }

        s3.writeJson(last_run_data, jsonFile)

        # generate audit report
        report = ""

        if len(invitesSent) > 0:
            report += f'Slack invitations have been sent to: \n'
            for user in invitesSent:
                report += f'\t{user[0]} at {user[1]} \n'
        else:
            report += f'No invitations need to be sent. \n'

        if len(doorAccessGranted) > 0:
            report += f'\nAccess to the door code channel has been granted to: \n'
            for user in doorAccessGranted:
                report += f'\t{user[0]} at {user[1]} \n'
        else:
            report +=f'\nNo users need to be added to the door channel. \n'

        if len(membersToRemove) > 0:
            report += f'\nThe following users are in slack and currently have no active subscription. Consider removing them: \n'
            for user in membersToRemove:
                report += f'\t{user[0]} at {user[1]} \n'
        else:
            report += f'\nNo users need to be removed from slack. \n'


        # send console log via email
        print("Slack Audit Function complete!")

        if "LOCAL-DEVELOPMENT" not in os.environ: #toggle local development options
            print("sending Audit report...")
            mailer.send_message("Daily Slack Audit", report)
        else:
            print(report)


    except Exception as e:
        print (e)

        # send console log via email
        mailer.send_message('slack_audit.py encountered an error', sys.stdout.log, ["getmoneygetpaid91@gmail.com"])
        raise 

if "LOCAL-DEVELOPMENT" in os.environ: #toggle local development options
    handler({}, {})