from setuptools import setup

setup(name='shared_scripts',
      version='0.1',
      description='Python scripts to be shared across the RCL Serverless Application',
      author='Aaron Peterson',
      packages=['shared_scripts'],
      install_requires=['boto3'],
      zip_safe=True)
