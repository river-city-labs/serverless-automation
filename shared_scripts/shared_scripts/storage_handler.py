import json
import boto3
from botocore.errorfactory import ClientError
from shared_scripts.login_handler import LoginHandler

class StorageHandler (object):
    s3 = None
    aws_bucket = 'rcl-function-storage'
    aws_key = None
    aws_secret = None
    logins = LoginHandler.instance()

    def __init__ (self):
        self.s3 = boto3.client(
            's3',
            aws_access_key_id= self.logins.getAwsKey(),
            aws_secret_access_key= self.logins.getAwsSecret()
        )
    
    def JsonExists (self, filename):
        try:
            self.s3.head_object(Bucket=self.aws_bucket, Key=filename)
            print(f"[storage_handler] {filename} exists")
            return True
        except ClientError:
            print(f"[storage_handler] {filename} does not exist")
            return False

    def readJson (self, filename):
        try:
            self.s3.head_object(Bucket=self.aws_bucket, Key=filename)
            print(f"[storage_handler] Downloading {filename}...")
            result = self.s3.get_object(Bucket=self.aws_bucket, Key=filename) 
            result = result["Body"].read().decode()
            return json.loads(result)
        except ClientError as e:
            raise Exception(f'Failed to read {filename}  from s3. Halting execution. {e}')

    def writeJson (self, data, filename):
        try:
            print(f"[storage_handler] writing {filename} to s3")
            self.s3.put_object(
                Body=json.dumps(data), 
                Bucket=self.aws_bucket, 
                Key=filename
            )
            return True
        except ClientError as e:
            raise Exception(f'Failed to write {filename} to s3. Halting execution. {e}')