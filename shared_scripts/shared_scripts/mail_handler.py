
import requests

from shared_scripts.login_handler import LoginHandler

class EmailHandler (object):
    logins = LoginHandler.instance()

    def send_message(self, subject, body, to=["rivercitylab@gmail.com"]):
        try:
            response = requests.post(
                "https://api.mailgun.net/v3/mg.rivercitylabs.space/messages",
                auth=("api", str(self.logins.getMailgunApiKey())),
                data={"from": "RCL Admin <noreply@rivercitylabs.space>",
                    "to": to,
                    "subject": str(subject),
                    "text": str(body)})
            print(response)
            return True
        except:
            return False