import os
import sys
from shared_scripts.decorators import Singleton
from shared_scripts.log_handler import LogHandler

@Singleton
class LoginHandler (object):
    gpw_pw = None
    aws_key = None
    aws_secret = None
    slack_api_token = None
    slack_legacy_token = None
    mailgun_api_key = None
    team_id = None

    def __init__ (self):
        sys.stdout = LogHandler.instance()
        
        # If running locally, initialize passwords from .env file
        if "LOCAL-DEVELOPMENT" in os.environ:
            print("[login_handler] using local environment variables for passwords")
            self.gpw_pw = os.getenv('GPW_PW')
            self.aws_key = os.getenv('AWS_KEY')
            self.aws_secret = os.getenv('AWS_SECRET')
            self.slack_api_token = os.getenv('SLACK_API_TOKEN')
            self.slack_legacy_token = os.getenv('SLACK_LEGACY_TOKEN')
            self.mailgun_api_key = os.getenv('MAILGUN_API_KEY')
            self.team_id = os.getenv('TEAM_ID')
        else:
            # We will try to interpolate passwords using gitlab secrets during deploy
            print("[login_handler] using production variables for passwords")
            self.gpw_pw = '#{GPWPW}'
            self.aws_key = '#{AWSKEY}'
            self.aws_secret = '#{AWSSECRET}'
            self.slack_api_token = '#{SLACKAPITOKEN}'
            self.slack_legacy_token = '#{SLACKLEGACYTOKEN}'
            self.mailgun_api_key = '#{MAILGUNAPIKEY}'
            self.team_id = '#{TEAMID}'

    def getGoPayWallPass (self):
        return self.gpw_pw

    def getAwsKey (self):
        return self.aws_key
        
    def getAwsSecret (self):
        return self.aws_secret

    def getSlackToken (self):
        return self.slack_api_token

    def getSlackLegacyToken (self):
        return self.slack_legacy_token

    def getMailgunApiKey (self):
        return self.mailgun_api_key

    def getTeamId (self):
        return self.team_id