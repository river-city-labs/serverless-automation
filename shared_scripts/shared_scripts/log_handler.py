import sys

from shared_scripts.decorators import Singleton

@Singleton
class LogHandler(object):
    def __init__(self):
        self.orgstdout = sys.stdout
        self.log = ""

    def write(self, msg):
        self.orgstdout.write(msg)
        self.log += msg
    
    def flush(self):
        self.orgstdout.flush()